FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=3.6.3
RUN apt-get update -y && \
    apt-get install -y dos2unix && \
    rm -r /var/cache/apt /var/lib/apt/lists

RUN wget https://github.com/SergiX44/XBackBone/releases/download/${VERSION}/release-v${VERSION}.zip -O release.zip && \
    unzip /app/code/release.zip && \
    rm -f /app/code/release.zip

COPY script-iiif.sh /app/code/script-iiif.sh
RUN bash /app/code/script-iiif.sh

# the chown is needed because htaccess has +SymLinksIfOwnerMatch
RUN ln -s /app/data/config.php /app/code/config.php && \
    rm -rf /app/code/logs && ln -s /run/xbackbone/logs /app/code/logs && \
    rm -rf /app/code/resources/cache && ln -s /run/xbackbone/cache /app/code/resources/cache && \
    rm -rf /app/code/resources/sessions && ln -s /run/xbackbone/sessions /app/code/resources/sessions && \
    mv /app/code/static/bootstrap/css/bootstrap.min.css /app/code/static/bootstrap/css/bootstrap.min.css.original && \
    ln -s /app/data/bootstrap.min.css /app/code/static/bootstrap/css/bootstrap.min.css && \
    chown -R www-data:www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/xbackbone.conf /etc/apache2/sites-enabled/xbackbone.conf
RUN echo "Listen 80" > /etc/apache2/ports.conf

RUN a2enmod rewrite

RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini 'mail function' sendmail_path /app/pkg/sendmail-swaks

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

ADD sendmail-swaks start.sh config.php /app/pkg/

CMD [ "/app/pkg/start.sh" ]
