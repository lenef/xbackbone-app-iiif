#!/bin/bash

set -eu

mkdir -p /app/data/resources/database 
mkdir -p /app/data/storage 
mkdir -p /run/xbackbone/{logs,cache,sessions}

function setup() {
    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "==> Waiting for apache2 to start"
        sleep 1
    done

    echo "==> Running first time setup"
    dsn="host=${CLOUDRON_MYSQL_HOST};port=${CLOUDRON_MYSQL_PORT};dbname=${CLOUDRON_MYSQL_DATABASE}"
    if ! curl --fail -c /tmp/cookiefile -v http://localhost:80/install/ -X POST --data "base_url=http://localhost:8000&connection=mysql&dsn=${dsn}&db_user=${CLOUDRON_MYSQL_USERNAME}&db_password=${CLOUDRON_MYSQL_PASSWORD}&storage_driver=local&storage_path=/app/data/storage&email=admin@cloudron.local&password=changeme"; then
        echo "==> Setup failed"
    fi

    gosu www-data:www-data cp /app/pkg/config.php /app/data/config.php

    echo "==> Setup complete"
}

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

chown -R www-data:www-data /app/data /run/xbackbone

if [[ ! -f /app/data/config.php ]]; then
    gosu www-data:www-data cp /app/code/static/bootstrap/css/bootstrap.min.css.original /app/data/bootstrap.min.css
    setup &
else
    echo "==> Running migrations"
    gosu www-data:www-data php /app/code/bin/migrate
    gosu www-data:www-data php /app/code/bin/clean
fi

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
