#!/bin/bash

set -eu

# Detournement du champ de personnalisation du header
# pour saisir l'adresse du serveur IIIF associe

sed -i "s/{{ customHead|raw }}//" /app/code/resources/templates/base.twig

sed -i "s/lang('custom_head_html')/lang('iiif_server')/; s/lang('custom_head_html_hint')/lang('iiif_server_hint')/" /app/code/resources/templates/dashboard/system.twig
sed -i "8i 'iiif_server' => 'IIIF Server'," /app/code/resources/lang/en.lang.php
sed -i "9i 'iiif_server_hint' => 'IIIF Server end point, for example : https://my-server/iiif/3/'," /app/code/resources/lang/en.lang.php
sed -i "8i 'iiif_server' => 'Serveur IIIF'," /app/code/resources/lang/fr.lang.php
sed -i "9i 'iiif_server_hint' => 'Point d\\\'entrée du serveur IIIF, par example : https://my-server/iiif/v3/'," /app/code/resources/lang/fr.lang.php

# Ajout du type mime pour les images JPEG2000 
sed -i "/'image\/jpeg',/a'image\/jp2'," /app/code/app/helpers.php

# Supression des icones dans l'affichage en liste
# Pour eviter une surcharge serveur et l'absence de vignettes .jp2

# sed -i "s/{{ lang('preview') }}//; s/isDisplayableImage(media.mimetype)/false/" /app/code/resources/templates/dashboard/list.twig 

# Construction du lien iiif info.json passant par le serveur IIIF
# Changement du mode d'affichage des images

si="{% if isDisplayableImage(media.mimetype) %}"
sinon="{% else  %}"
fin="{% endif  %}"


str="{{ urlFor(glue(media.user_code, media.code) ~ (copy_raw ? '\/raw.' ~ media.extension : '.' ~ media.extension)) }}"
nvl="{{ customHead|raw }}{{ media.user_code ~ '%2F' ~ media.code ~ '.' ~ media.extension ~ '\/info.json' }}"

rep="$si$nvl$sinon$str$fin"

lang="{{ lang('copy_link') }}"
replang="$si$lang IIIF$sinon$lang$fin"

sed -i "s/$str/$rep/; s/$lang/$replang/" /app/code/resources/templates/dashboard/list.twig
sed -i "s/$str/$rep/; s/$lang/$replang/" /app/code/resources/templates/dashboard/grid.twig

si="{% if type == 'image' %}"
rep="$si$nvl$sinon$str$fin"
replang="$si$lang IIIF$sinon$lang$fin"

# Visualiseur IIIF OpenSeaDragon

img="<img src=\"{{ url }}\/raw\" class=\"img-thumbnail rounded mx-auto d-block\" alt=\"{{ media.filename }}\">"
viewer="<div id='openseadragon1' style='width: 100%; height: 650px;'><\/div><script src='https:\/\/cdn.jsdelivr.net\/npm\/openseadragon@4.0\/build\/openseadragon\/openseadragon.min.js'><\/script><script type='text\/javascript'>var viewer = OpenSeadragon({id: 'openseadragon1',prefixUrl: 'https:\/\/cdn.jsdelivr.net\/npm\/openseadragon@4.0\/build\/openseadragon\/images\/',tileSources: '{{ customHead|raw }}{{ media.user_code ~ \'%2F\' ~ media.code ~ \'.\' ~ media.extension ~ \'\/info.json\' }}'});<\/script>"

sed -i "s/$str/$rep/; s/$lang/$replang/; s/$img/$viewer/" /app/code/resources/templates/upload/public.twig

