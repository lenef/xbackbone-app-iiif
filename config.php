<?php

return [
    'app_name' => 'XBackBone',
    'base_url' => getenv('CLOUDRON_APP_ORIGIN'),
    'db'       => [
        'connection' => 'mysql',
        'dsn'        => 'host=' . getenv('CLOUDRON_MYSQL_HOST') . ';port=' . getenv('CLOUDRON_MYSQL_PORT') . ';dbname=' . getenv('CLOUDRON_MYSQL_DATABASE'),
        'username'   => getenv('CLOUDRON_MYSQL_USERNAME'),
        'password'   => getenv('CLOUDRON_MYSQL_PASSWORD'),
    ],
    'storage' => [
        'driver' => 'local',
        'path'   => '/app/data/storage',
    ],
    'ldap' => array(
        'enabled' => getenv('CLOUDRON_LDAP_SERVER') !== false, // enable it
        'host' => getenv('CLOUDRON_LDAP_SERVER'), // set the ldap host
        'port' => intval(getenv('CLOUDRON_LDAP_PORT')), // ldap port
        'base_domain' => getenv('CLOUDRON_LDAP_USERS_BASE_DN'), // the base_dn string
        'search_filter' => '(&(objectclass=user)(username=????))', // ???? is replaced with user provided username
        'rdn_attribute' => 'username', // the attribute to use as username
        'service_account_dn' => getenv('CLOUDRON_LDAP_BIND_DN'), // LDAP Service Account Full DN
        'service_account_password' => getenv('CLOUDRON_LDAP_BIND_PASSWORD')
    ),
];

